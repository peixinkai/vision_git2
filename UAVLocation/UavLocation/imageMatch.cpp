#include "stdafx.h"
#include "common.h"
#include "imageMatch.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>


int tran_tab[48] = { 0,1,
	1,2,
	2,3,
	3,4,
	4,5,
	5,6,
	6,7,
	7,0,
	0,2,
	1,3,
	2,4,
	3,5,
	4,6,
	5,7,
	6,0,
	7,1,
	0,3,
	1,4,
	2,5,
	3,6,
	4,7,
	5,0,
	6,1,
	7,2 };

int bits_count_tab[256];

OPENCV_DLL_API void bits_count_tab_init()
{
	for (int i = 0; i < 256; i++)
	{
		int v = 0;
		for (int j = 0; j < 8; j++)
			if (i & 1 << j) v++;
		bits_count_tab[i] = v;
	}
}


void HogCompute(IplImage *inputImg, float *hogs, int bins, int bsize)
{
	IplImage * Gm = cvCreateImage(cvGetSize(inputImg), IPL_DEPTH_32F, 1);
	cvZero(Gm);
	IplImage * Gb = cvCreateImage(cvGetSize(inputImg), IPL_DEPTH_32F, 1);
	cvZero(Gb);
	IplImage *grayImg = cvCreateImage(cvGetSize(inputImg), IPL_DEPTH_8U, 1);
	inputImg->nChannels == 3 ? cvCvtColor(inputImg, grayImg, CV_RGB2GRAY) : cvCopy(inputImg, grayImg);

	int w = 2;
	IplImage * tempImg = cvCreateImage(cvSize(inputImg->width + w, inputImg->height + w), 8, 1);
	cvCopyMakeBorder(grayImg, tempImg, cvPoint(w / 2, w / 2), IPL_BORDER_REFLECT);

	IplImage * gx = cvCreateImage(cvSize(grayImg->width + w, grayImg->height + w), IPL_DEPTH_32F, 1);
	IplImage * gy = cvCreateImage(cvSize(grayImg->width + w, grayImg->height + w), IPL_DEPTH_32F, 1);
	cvSmooth(tempImg, tempImg, CV_GAUSSIAN);
	cvSobel(tempImg, gx, 1, 0, 1); cvSobel(tempImg, gy, 0, 1, 1);

	for (int i = 0; i < Gm->height; i++)
	{
		float * por = (float *)(Gb->imageData + i * Gb->widthStep);
		float * pmg = (float *)(Gm->imageData + i * Gm->widthStep);
		float * pgx = (float *)(gx->imageData + (i + w / 2) * gx->widthStep);
		float * pgy = (float *)(gy->imageData + (i + w / 2) * gy->widthStep);

		for (int j = 0; j < Gm->width; j++)
		{
			//get the angle in the range [-pi, pi]
			por[j] = atan2(pgy[j + w / 2], pgx[j + w / 2]);
			//map to [-pi/2, pi/2]
			if (por[j] <= -PI / 2.0f)
				por[j] = por[j] + PI;
			else if (por[j] > PI / 2.0f)
				por[j] = por[j] - PI;
			else
			{
			}
			//get the magnitude
			pmg[j] = abs(pgx[j + w / 2]) + abs(pgy[j + w / 2]);
		}
	}

	IplImage * tGm = cvCloneImage(Gm);
	IplImage * integralImg = cvCreateImage(cvSize(tGm->width + 1, tGm->height + 1), IPL_DEPTH_32F, 1);
	for (int b = 0; b < bins; b++)
	{
		cvCopy(Gm, tGm);
		for (int i = 0; i < Gm->height; i++)
		{
			float * pgm = (float *)(tGm->imageData + i * tGm->widthStep);
			float * pgb = (float *)(Gb->imageData + i * Gb->widthStep);
			for (int j = 0; j < Gm->width; j++)
			{
				if (pgm[j])
					pgm[j] = pgm[j];
				int v = (int)(((pgb[j] + PI / 2.0) / PI) * bins);
				if (v != b)
					pgm[j] = 0.0f;
			}
		}

		cvIntegral(tGm, integralImg);
		int h = inputImg->height - bsize + 1;
		int w = inputImg->width - bsize + 1;
		for (int y = 0; y < h; y++)
		{
			float * py1 = (float *)(integralImg->imageData + y * integralImg->widthStep);
			float * py2 = (float *)(integralImg->imageData + (y + bsize) * integralImg->widthStep);
			for (int x = 0; x < w; x++)
			{
				int hindex = (y * w + x) * bins;
				hogs[hindex + b] = py2[x + bsize] - py2[x] - py1[x + bsize] + py1[x];
			}
		}

	}
	cvReleaseImage(&integralImg); cvReleaseImage(&tGm);
	cvReleaseImage(&Gm); cvReleaseImage(&Gb); cvReleaseImage(&grayImg);
	cvReleaseImage(&tempImg); cvReleaseImage(&gx); cvReleaseImage(&gy);
}


void PQ_HOG_Transform(IplImage *inputImg, IplImage *HOGimg, int bsize)
{
	cvZero(HOGimg);
	int step, blocksize, cellsize, bins;
	step = 1; blocksize = bsize; cellsize = bsize; bins = 8;
	IplImage *grayImg = cvCreateImage(cvGetSize(inputImg), IPL_DEPTH_8U, 1);
	inputImg->nChannels == 3 ? cvCvtColor(inputImg, grayImg, CV_RGB2GRAY) : cvCopy(inputImg, grayImg);

	IplImage * tempImg = cvCreateImage(cvSize(inputImg->width + blocksize - 1, inputImg->height + blocksize - 1), 8, 1);
	cvCopyMakeBorder(grayImg, tempImg, cvPoint(blocksize / 2, blocksize / 2), IPL_BORDER_REFLECT);

	float * hogs = (float *)malloc(sizeof(float) *  HOGimg->height *  HOGimg->width * bins);
	HogCompute(tempImg, hogs, bins, blocksize);
	for (int i = 0; i < HOGimg->height; i++)
	{
		BYTE * pHog = (BYTE *)(HOGimg->imageData + i * HOGimg->widthStep);
		for (int j = 0; j < HOGimg->width; j++)
		{
			int findex = i * HOGimg->width * bins + j * bins;
			for (int c = 0; c < HOGimg->nChannels; c++)
			{
				for (int b = 0; b < 8; b++)
				{
					float plusv = hogs[findex + tran_tab[c * 8 + 2 * b]];
					float negtv = hogs[findex + tran_tab[c * 8 + 2 * b + 1]];
					if (plusv > negtv)
						pHog[j * HOGimg->nChannels + c] |= 1 << b;
				}
			}
		}
	}
	free(hogs);
	cvReleaseImage(&tempImg); cvReleaseImage(&grayImg);
}

int hamdist(IplImage *HOGimg1, IplImage *HOGimg2, IplImage *MaskImg, int threshold)
{
	int num = 0;
	uchar* tmp= new uchar;
	for (int i = 0; i < HOGimg1->height; i++)
	{
		BYTE * ph1 = (BYTE *)(HOGimg1->imageData + i * HOGimg1->widthStep);
		BYTE * ph2 = (BYTE *)(HOGimg2->imageData + i * HOGimg2->widthStep);
		for (int j = 0; j < HOGimg1->width; j++)
		{
			int bitnum = 0;
			tmp = (uchar *)(MaskImg->imageData + i * MaskImg->widthStep +j);
			for (int c = 0; c < HOGimg1->nChannels; c++)
			{
				BYTE v = ~(ph1[j * HOGimg1->nChannels + c] ^ ph2[j * HOGimg2->nChannels + c]);
				bitnum += bits_count_tab[v];
			}
			if (bitnum > threshold)
				num = num+bitnum*(*tmp);
		}
	}
	return num;
}


CvPoint FullSearch_Hamming(IplImage * baseHogimg, IplImage * tHogimg, IplImage * cvresults, IplImage *MaskImg,int threshold,int step)
{
	IplImage * subimg = cvCloneImage(tHogimg);
	for (int i = 0; i < cvresults->height; i+=step)
	{
		float * pr = (float *)(cvresults->imageData + i * cvresults->widthStep);//指向当前行的首地址
		for (int j = 0; j < cvresults->width; j+=step)
		{
			cvSetImageROI(baseHogimg, cvRect(j, i, tHogimg->width, tHogimg->height));
			cvCopy(baseHogimg, subimg);
			cvResetImageROI(baseHogimg);
			//cvNamedWindow("result");
			//cvShowImage("result", tHogimg);
			//waitKey(0);
			pr[j] = (float)hamdist(subimg, tHogimg, MaskImg, threshold);
		}
	}
	cvReleaseImage(&subimg);
	double maxv; CvPoint maxp;
	cvMinMaxLoc(cvresults, NULL, &maxv, NULL, &maxp);
	//cvNamedWindow("result");
	//cvShowImage("result", cvresults);
	//waitKey(0);

	return maxp;
}


OPENCV_DLL_API void ImageMatching_PQHOG(Mat Mask,Mat Greal,Mat EGbase,\
	std::vector<cv::Point2f> & srcps,std::vector<cv::Point2f> & dstps)
{
	//int l = 80;
	int bsize = 8;
	int threshold = 0;

	IplImage *SenseImg_org = (IplImage *)&(IplImage(Greal));
	IplImage *BaseImg_org = (IplImage *)&(IplImage(EGbase));
	IplImage *maskImg_org = (IplImage *)&(IplImage(Mask));

	//转灰度
	IplImage *GraySimg = cvCreateImage(cvGetSize(SenseImg_org), IPL_DEPTH_8U, 1);
	SenseImg_org->nChannels == 3 ? cvCvtColor(SenseImg_org, GraySimg, CV_RGB2GRAY) : cvCopy(SenseImg_org, GraySimg);

	IplImage *GrayBimg = cvCreateImage(cvGetSize(BaseImg_org), IPL_DEPTH_8U, 1);
	BaseImg_org->nChannels == 3 ? cvCvtColor(BaseImg_org, GrayBimg, CV_RGB2GRAY) : cvCopy(BaseImg_org, GrayBimg);


	//裁剪出匹配的中间1500*1500区域
	CvRect roi = cvRect(500, 500, 2000, 2000);
	IplImage *BaseImg_cut = cvCreateImage(cvSize(roi.width, roi.height), GrayBimg->depth, GrayBimg->nChannels);
	cvSetImageROI(GrayBimg,roi);//获取感兴趣的区域 ROI:region of interesting
	cvCopy(GrayBimg,BaseImg_cut);
	//显示图像
	cvNamedWindow("1");
	cvShowImage("1", BaseImg_cut);
	cvWaitKey(0);//一定要有
	cvResetImageROI(GrayBimg);

	
	//图像压缩
	float scale_factor = 0.25;
	IplImage *BaseImg,*SenseImg,*MaskImg;
	CvSize base_sz,sense_sz;
	base_sz.width = cvRound(BaseImg_cut->width*scale_factor);
	base_sz.height = cvRound(BaseImg_cut->height*scale_factor);
	sense_sz.width = cvRound(GraySimg->width*scale_factor);
	sense_sz.height = cvRound(GraySimg->height*scale_factor);
	BaseImg = cvCreateImage(base_sz, BaseImg_cut->depth, BaseImg_cut->nChannels);
	SenseImg = cvCreateImage(sense_sz, GraySimg->depth, GraySimg->nChannels);
	MaskImg = cvCreateImage(sense_sz, maskImg_org->depth, maskImg_org->nChannels);
	cvResize(BaseImg_cut, BaseImg, CV_INTER_LINEAR);
	cvResize(GraySimg, SenseImg, CV_INTER_LINEAR);
	cvResize(maskImg_org, MaskImg, CV_INTER_NN);
	//显示图像
	namedWindow("1", CV_WINDOW_FREERATIO);
	cvNamedWindow("1");
	cvShowImage("1", BaseImg_cut);
	cvWaitKey(0);//一定要有

	IplImage * BaseHOGImg = cvCreateImage(cvGetSize(BaseImg), 8, 3);
	IplImage * SenseHOGImg = cvCreateImage(cvGetSize(SenseImg), 8, 3);

	//提取PQHOG特征
	PQ_HOG_Transform(BaseImg, BaseHOGImg, bsize);
	PQ_HOG_Transform(SenseImg, SenseHOGImg, bsize);

	CvPoint minp;
	int iw = BaseImg->width - SenseImg->width + 1;
	int ih = BaseImg->height - SenseImg->height + 1;
	IplImage * cvresults = cvCreateImage(cvSize(iw, ih), 32, 1);
	cvZero(cvresults);

	//使用PQHOG特征匹配，步长2
	minp = FullSearch_Hamming(BaseHOGImg, SenseHOGImg, cvresults, MaskImg, threshold,2);
	CvPoint minp_f;
	if ( minp.x*4-20 >= 0 && minp.y*4-20 >= 0 && GraySimg->width + 40 <= BaseImg_cut->width && GraySimg->height + 40 <= BaseImg_cut->height)
	{
		//在1500*1500基准图上所得定位点附近41*41范围再次检索
		CvRect roi_s = cvRect(minp.x*4-20, minp.y*4-20,GraySimg->width + 40, GraySimg->height + 40);
		IplImage *BaseImg_s = cvCreateImage(cvSize(roi_s.width, roi_s.height), BaseImg_cut->depth, BaseImg_cut->nChannels);
		cvSetImageROI(BaseImg_cut, roi_s);
		cvCopy(BaseImg_cut, BaseImg_s);
		cvResetImageROI(BaseImg_cut);
		//IplImage * SenseImg_s = cvCloneImage(GraySimg);
		IplImage * BaseHOGImg_s = cvCreateImage(cvGetSize(BaseImg_cut), 8, 3);
		IplImage * SenseHOGImg_s = cvCreateImage(cvGetSize(GraySimg), 8, 3);
		PQ_HOG_Transform(BaseImg_cut, BaseHOGImg_s, bsize);
		PQ_HOG_Transform(GraySimg, SenseHOGImg_s, bsize);
		IplImage * cvresults_s = cvCreateImage(cvSize(41, 41), 32, 1);
		CvPoint minp_s;
		cvZero(cvresults_s);
		minp_s = FullSearch_Hamming(BaseHOGImg_s, SenseHOGImg_s, cvresults_s, maskImg_org, threshold, 1);
		cvReleaseImage(&BaseHOGImg_s); cvReleaseImage(&SenseHOGImg_s); cvReleaseImage(&BaseImg_s);cvReleaseImage(&cvresults_s);
		minp_f.x = minp.x * 4 + minp_s.x - 20;
		minp_f.y = minp.y * 4 + minp_s.y - 20 ;
	}
	else
	{
		minp_f.x = minp.x * 4;
		minp_f.y = minp.y * 4;
	}
		//compare with the true positions and record the error matches.
	CvPoint mp;
	for (int i = 0; i < srcps.size(); i++)
	{
		mp.x = minp_f.x + srcps[i].x +500;
		mp.y = minp_f.y + srcps[i].y +500;
		dstps.push_back(mp);
	}

	cvReleaseImage(&cvresults);   
	cvReleaseImage(&BaseImg_cut);
	cvReleaseImage(&BaseImg); 	
	cvReleaseImage(&SenseImg); 
	cvReleaseImage(&MaskImg);
	cvReleaseImage(&GraySimg); cvReleaseImage(&GrayBimg);
	cvReleaseImage(&BaseHOGImg); cvReleaseImage(&SenseHOGImg);
}
