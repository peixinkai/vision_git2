#ifndef __COMMON__
#define __COMMON__

#include <cv.h>
#include <math.h>
#include<list>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <io.h>
//#include <tbb/tbb.h>
//#include <opencv2/core/opengl_interop.hpp>
#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/contrib/contrib.hpp>
//#include "opencv2/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/photo/photo.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/video/tracking.hpp"
//#include "opencv2/xfeatures2d.hpp"


using namespace std;
using namespace cv;
//#define HAVE_TBB 1
//
//#ifdef HAVE_TBB
//using namespace tbb;
//#endif


typedef unsigned char BYTE;

void MyErrorExit(const char * message);

void BYTE2Mat(BYTE *imgData, Mat &mimg);

void Mat2CBYTE(Mat mimg, BYTE * imgData);

void Drawcorss(Mat img, int x, int y, int cr);

double str2double(string str);

std::vector<std::string> strsplit(std::string str,std::string pattern);

void getFiles( string path, vector<string>& files );

#define FPS_COUNT 0
//#define GROUND_TEST 1
//#define OLD_V 0
//#define USE_GPU 0

#define PI				3.141592653590
#define R2D				57.295779513082
#define Deg2Rad( deg )	(deg)/ R2D		
#define Rad2Deg( Rad )	(Rad)* R2D		

#endif