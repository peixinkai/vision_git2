#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <Windows.h>
#include <process.h>  
#include <stdexcept>
#include "common.h"
#include "imageMatch.h"
#include "imageRectify.h"

//#define GROUND_TEST 1

#ifdef _DEBUG
#pragma comment( lib, "opencv_calib3d249d.lib")	
#pragma comment( lib, "opencv_contrib249d.lib")	
#pragma comment( lib, "opencv_core249d.lib")	
#pragma comment( lib, "opencv_features2d249d.lib")	
#pragma comment( lib, "opencv_flann249d.lib")	
#pragma comment( lib, "opencv_highgui249d.lib")	
#pragma comment( lib, "opencv_imgproc249d.lib")	
#pragma comment( lib, "opencv_legacy249d.lib")	
#pragma comment( lib, "opencv_video249d.lib")	

//#pragma comment( lib, "opencv_world320d.lib")	 
//#pragma comment( lib, "ImageMatchD.lib")	
#else
#pragma comment( lib, "opencv_calib3d249.lib")	
#pragma comment( lib, "opencv_contrib249.lib")	
#pragma comment( lib, "opencv_core249.lib")	
#pragma comment( lib, "opencv_features2d249.lib")	
#pragma comment( lib, "opencv_flann249.lib")	
#pragma comment( lib, "opencv_highgui249.lib")	
#pragma comment( lib, "opencv_imgproc249.lib")	
#pragma comment( lib, "opencv_legacy249.lib")	
#pragma comment( lib, "opencv_video249.lib")
#endif



FILE * logFile = NULL;
#define MAX_EULARP 10.0
#define MAX_EULARR 10.0

//功能：检测滚转和俯仰的角度是否过大
//输入：滚转角，单位：度（EularR），俯仰角，单位：度（EularP）
//输出/返回结果：过大返回true,否则返回false
bool check_pitch_roll(double EularP,double EularR)
{
	if (abs(EularP) < MAX_EULARP && abs(EularR) < MAX_EULARR)
		return true;
	else
		return false;
}

//功能：在给定图像画圆点（多个）
//输入：输入图像(img),  圆点位置(srcps)，个数； r 圆点半径大小，sc 颜色
void Drawps(Mat img,std::vector<cv::Point2f> srcps, int r, Scalar sc)
{

	for (int i = 0; i < srcps.size(); i++)
	{
		CvPoint p;
		p.x = cvRound(srcps[i].x);
		p.y = cvRound(srcps[i].y);
		circle(img,p,r,sc,2);
	}
}

//利用基准点的对应关系，解算出坐标变换，将坐标从米装换为度
//输入：使用米坐标的基准点4个（baseMps），使用度坐标的基准点4个（baseDps）
//输入：使用米坐标的需要转换坐标的点（Mpt）。
//输出：得到使用度坐标的点（Dpt），即Mpt的装换结果。
void meter2degree_multi(std::vector<cv::Point2f> baseMps,\
	std::vector<cv::Point2f> baseDps, Point2f Mpt,Point2f &Dpt)
{
	std::vector<cv::Point2f> srcps, dstps;
	//利用基准点对应，解算变换矩阵H（米到度）
	Mat H = getPerspectiveTransform(baseMps,baseDps);
	srcps.push_back(Mpt);
	//将Mpt从米装换到度
	perspectiveTransform(srcps,dstps,H);
	Dpt = dstps[0];
	return;
}

//利用基准点的对应关系，解算出坐标变换，将坐标从度装换为米
//输入：使用米坐标的基准点4个（baseMps），使用度坐标的基准点4个（baseDps）
//输入：使用米坐标的需要转换坐标的点（Dpt）。
//输出：得到使用度坐标的点（Mpt），即Dpt的装换结果。
void degree2meter_multi(std::vector<cv::Point2f> baseMps,\
	std::vector<cv::Point2f> baseDps, Point2f Dpt,Point2f &Mpt)
{
	std::vector<cv::Point2f> srcps, dstps;
	//利用基准点对应，解算变换矩阵H（度到米）
	Mat H = getPerspectiveTransform(baseDps,baseMps);
	//将Dpt从米装换到度
	srcps.push_back(Dpt);
	perspectiveTransform(srcps,dstps,H);
	Mpt = dstps[0];
	return;
}


void main()
{
	//以系统时间命名日志文件并且打开
	char fname[256];
	SYSTEMTIME time;
	GetLocalTime(&time);
	sprintf(fname, "Log%d_%d_%d_%d_%d_%d_%d.txt",\
		time.wYear,time.wMonth, time.wDay,time.wHour,\
		time.wMinute,time.wSecond,time.wMilliseconds);
	logFile = fopen(fname,"w");
	if (logFile == NULL) MyErrorExit("main::log file open failed.");
	printf("日志文件打开成功.\n");


	//读取ins_img数据文件相关变量,打开文件
	int INS_CNT; int IMG_CNT; double EXP_US;
	double GAIN_DB; double LUM; double TEMP_GRAY;
	double UTC;  double GPSTIME; int SATNUM;
	double ROLL,PITCH, YAW, V_X, V_Y, V_Z, LAT, LON, ALT,\
		TEMP_INS, ACCEL_X, ACCEL_Y, ACCEL_Z, GYRO_X, GYRO_Y, GYRO_Z, HDT,\
		m_altitude,EularR,EularP,EularY;
	FILE *fp = fopen("C:\\pxk\\长沙下视匹配\\1\\1\\ins_img.txt","r");
	if (!fp) MyErrorExit("main::ins_img.txt fopen failed.");

	//设定飞行高度（相对地面）
	//m_altitude = 378;
	m_altitude = 280;
	//十进制度与米转换的基准点
	std::vector<cv::Point2f> baseMps, baseDps;
	//Point2f pt1,pt2,pt3,pt4;
	//pt1.y = 3120176.5010000002; pt1.x = 680537.37399999995;  baseMps.push_back(pt1);
	//pt2.y = 3120176.5010000002; pt2.x = 683537.37399999995;  baseMps.push_back(pt2);
	//pt3.y = 3117176.5010000002; pt3.x = 680537.37399999995;  baseMps.push_back(pt3);
	//pt4.y = 3117176.5010000002; pt4.x = 683537.37399999995;  baseMps.push_back(pt4);
	//pt1.y = 28.195029093; pt1.x = 112.839321517;   baseDps.push_back(pt1);
	//pt2.y = 28.194615004; pt2.x = 112.86987242;  baseDps.push_back(pt2);
	//pt3.y = 28.167961924; pt3.x = 112.838858291; baseDps.push_back(pt3);
	//pt4.y = 28.167548302; pt4.x = 112.869401512;  baseDps.push_back(pt4);
	//黄桥
	Point2f pt1,pt2,pt3,pt4;
	pt1.y = 3120176.5010000002; pt1.x = 675537.37399999995;  baseMps.push_back(pt1);
	pt2.y = 3120176.5010000002; pt2.x = 678537.37399999995;  baseMps.push_back(pt2);
	pt3.y = 3117176.5010000002; pt3.x = 675537.37399999995;  baseMps.push_back(pt3);
	pt4.y = 3117176.5010000002; pt4.x = 678537.37399999995;  baseMps.push_back(pt4);
	pt1.y = 28.1957041; pt1.x = 112.788401923;   baseDps.push_back(pt1);
	pt2.y = 28.195301368; pt2.x = 112.81895389;  baseDps.push_back(pt2);
	pt3.y = 28.168636169; pt3.x = 112.787951505; baseDps.push_back(pt3);
	pt4.y = 28.16823389; pt4.x = 112.818495788;  baseDps.push_back(pt4);

	//得到基准图
	struct BaseData bdata;
	//bdata.top = 3120176;
	//bdata.left = 680537;
	bdata.top = 3120176;
	bdata.left = 675537;
	bdata.baseImg = imread("1.jpg",0);

	//匹配算法查表初始化
	bits_count_tab_init();
	std::vector<cv::Point2f> srcps, reps, dstps;
	std::vector<cv::Point2f> resultPs,GPSPs;
	double ave_error = 0;
	int cn = 0;  int error_cn = 0; 	int old_cnt = 0;
	double matchLon,matchLat,matchH;
	Mat ImgCopy;

	for (int i = 0; i < 99999; i++)
	{
		//读取ins_img文件
		fscanf(fp,"%d %d %lf %lf %lf %lf %lf %lf %d %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf %lf",\
			&INS_CNT, &IMG_CNT, &EXP_US, &GAIN_DB, &LUM, &TEMP_GRAY, &UTC, &GPSTIME, &SATNUM, &ROLL, &PITCH, &YAW, &V_X, &V_Y, &V_Z, &LAT, &LON, &ALT, &TEMP_INS, &ACCEL_X, &ACCEL_Y, &ACCEL_Z, &GYRO_X, &GYRO_Y, &GYRO_Z, &HDT);
		//读取指定编号范围内的图像
		if (IMG_CNT >= 222 &&  IMG_CNT <= 369)
		{
			if (old_cnt < IMG_CNT)
			{
				old_cnt = IMG_CNT;
				//读取实时图
				sprintf(fname,"C:\\pxk\\长沙下视匹配\\1\\1\\IMG_000%d.jpg",IMG_CNT);
				Mat img = imread(fname);
				//得到三轴姿态
				EularR = ROLL; EularP = PITCH; EularY = YAW;
				//检查俯仰和滚转是否过大，太大则读下张图
				if(!check_pitch_roll(EularP,EularR)) continue;
			
				//实时图转灰度
				Mat grayImg;
				img.channels() == 3 ?  cvtColor(img,grayImg,CV_RGB2GRAY) : grayImg = img;
				//判读图像是否过暗，太暗（平均灰度小于20）则跳过
				Scalar mgray = mean(grayImg);
				double mv  = mgray.val[0];
				if (mv < 20) continue;
				
				//根据相对地面高度计算尺度调整比例（地面分辨率应该与基准图一致）
				double scale_ratio =  zoom_etimation(m_altitude);//比例尺
				
				//实时图畸变矫正
				Mat undistortedImg, remapImg;
				undistortedImg = remapTrim(grayImg);
				
				//生成mask
				Mat fullmask = Mat(grayImg.size(),CV_8UC1);
				/*imshow("效果图",fullmask);
				waitKey(0);*/
				fullmask.setTo(1); 
				fullmask = remapTrim(fullmask);
				/*imshow("效果图",fullmask);
				waitKey(0);*/
				//保存mask的图
				//ImgCopy = fullmask.clone();
				//Drawps(ImgCopy,srcps,10,Scalar(255,255,255));
				//imwrite("mask.jpg",ImgCopy);
				//ImgCopy.release();
			
				//选择9个匹配点
				float intervalX = undistortedImg .cols / 4.0f;
				float intervalY = undistortedImg .rows / 4.0f;
				for (int i = 1; i <= 3; i ++)
				{
					for (int j = 1; j <= 3; j++)
					{
						Point2f p;
						p.x = j * intervalX; p.y = i * intervalY;
						srcps.push_back(p);
					}
				}
				//保存矫正完图像，和匹配点
				//ImgCopy = undistortedImg.clone();
				//Drawps(ImgCopy,srcps,10,Scalar(255,255,255));
				//imwrite("UndistortedImg.jpg",ImgCopy);
				//ImgCopy.release();

				//根据三轴姿态矫正实时图
			    //Mat H = RotationHomographyEstimation(EularP,-EularR,EularY);
				Mat H = RotationHomographyEstimation(0,0,EularY);
				//调整实时图的尺度缩放，并获得最小外接矩
				Size s = undistortedImg.size();
				Mat H2 = H_re_estimation(s,H,scale_ratio);
			/*	imshow("效果图",undistortedImg);
				waitKey(0);*/
				warpPerspective(undistortedImg,remapImg, H2, s,INTER_CUBIC);//插值方法为什么是INTER_CUBIC？
				/*imshow("效果图",remapImg);
				waitKey(0);*/
				//根据上一步计算的变换矩阵，变换匹配点
				perspectiveTransform(srcps,reps,H2);

				//保存矫正好的实时图
				ImgCopy = remapImg.clone();
				Drawps(ImgCopy,reps,3,Scalar(255,255,255));
				sprintf(fname,"C:\\pxk\\长沙下视匹配\\1\\RectifiedImg%d.jpg",IMG_CNT);
				imwrite(fname,ImgCopy);
				ImgCopy.release();

				//变换mask
				Mat mask;
				warpPerspective(fullmask,mask,H2,s,INTER_NEAREST);//为什么这里的插值方法是INTER_NEAREST
				/*imshow("效果图",fullmask);
				waitKey(0);	
				imshow("效果图",mask);
				waitKey(0);		*/
				//对mask进行腐蚀，解决算法的边沿问题
				Mat element = getStructuringElement(MORPH_RECT,Size(11,11));//如果不处理对后面的匹配有哪些影响
				cv::erode(mask,mask,element);
				
				//保存矫正好的mask
				//ImgCopy = mask.clone();
				//cv::normalize(mask,ImgCopy,255,0,NORM_MINMAX );
				//imwrite("C:\\datas\\UAVLocation\\3\\maskImg.jpg",ImgCopy);
				//ImgCopy.release();

				//图像匹配
				//输入：reps(9个匹配点在实时图上的位置)
				//输出：dstps(9个匹配点在基准图上的位置)
			//	ImageMatching(mask,remapImg,bdata.baseImg,reps,dstps);
				double t = (double) cv::getTickCount(); 
				ImageMatching_PQHOG(mask,remapImg,bdata.baseImg,reps,dstps);
				t = ((double) cv::getTickCount() - t) / cv::getTickFrequency(); 

				//在基准图上画出9个点的匹配位置，然后保存基准图
				ImgCopy = bdata.baseImg.clone();
				Drawps(ImgCopy,dstps,10,Scalar(255,255,255));
				sprintf(fname,"C:\\pxk\\长沙下视匹配\\1\\MatchrResult%d.jpg",IMG_CNT);
				imwrite(fname,ImgCopy);
				ImgCopy.release();


				std::vector<cv::Point3f> objectPoints;
				std::vector<cv::Point2f> imagePoints;
				//从基准图坐标转到地理坐标单位（米），保存在objectPoints
				for (int i = 0; i < dstps.size(); i++)
				{
					dstps[i].x =(float) bdata.left + dstps[i].x;
					dstps[i].y = (float) bdata.top - dstps[i].y;
				}
				for (int i = 0; i < dstps.size(); i++)
				{
					Point3f p;
					p.x = dstps[i].x; p.y = dstps[i].y; p.z = 0.0; 
					objectPoints.push_back(p);
				}
				//实时图（畸变矫正后）上9个点的图像坐标保存在imagePoints
				for (int i = 0; i < srcps.size(); i++)
				{
					Point2f p; 
					p.x = srcps[i].x; 
					p.y = srcps[i].y;
					imagePoints.push_back(p);
				}


				//PNP,相机参数
				float camD[9] = { FLENX, 0, PX, 0, FLENY, PY, 0, 0, 1 };
				cv::Mat camera_matrix = cv::Mat(3, 3, CV_32FC1, camD);
				//不使用畸变参数
				float distCoeffD[5] = { 0.0, 0.0, 0.0, 0.0, 0.0 };
				cv::Mat distortion_coefficients = cv::Mat(5, 1, CV_32FC1, distCoeffD);

				//初始化输出矩阵
				cv::Mat rvec = cv::Mat::zeros(3, 1, CV_32FC1); 
				cv::Mat tvec = cv::Mat::zeros(3, 1, CV_32FC1);
				cv::Mat outdate = cv::Mat::zeros(3, 1, CV_32FC1);
				//根据9个点的实时图像上坐标（imagePoints），地理坐标（objectPoints），
				//相机参数（camera_matrix），畸变参数（distortion_coefficients），解算相机位姿
				solvePnPRansac(objectPoints, imagePoints, camera_matrix, distortion_coefficients, rvec, tvec, false, 100, 8.0, 0.99, outdate,CV_ITERATIVE);
				Mat _R_matrix = cv::Mat::zeros(3, 3, CV_32FC1);   // rotation matrix 
				Mat _t_matrix = cv::Mat::zeros(3, 1, CV_32FC1);   // translation matrix 
				// Transforms Rotation Vector to Matrix
				Rodrigues(rvec,_R_matrix); //得到旋转矩阵
				_t_matrix = tvec;   //得到得到位移矩阵,即是世界坐标W原点在相机坐标C的位置
				cv::Mat point3d = cv::Mat(3, 1, CV_64FC1);
				point3d = _R_matrix.inv() * _t_matrix; //世界坐标W原点在相机坐标C'(相机坐标C'与相机坐标系C同原点，与世界坐标同方向)的位置
				point3d = -point3d; //得到相机坐标C'和C的原点在世界坐标系的位置
				
				Point2f Mpt,Dpt;
				Mpt.x = point3d.at<double>(0);
				Mpt.y = point3d.at<double>(1);
				//将相机的水平位置从米转换成度
				meter2degree_multi(baseMps,baseDps,Mpt,Dpt);
				//得到经度、纬度、高度
				matchLon = Dpt.x; matchLat = Dpt.y; matchH = point3d.at<double>(2);

				//将匹配结果转到基准图坐标（基准图分辨率是1米/像素）
				Point2f rsp,gps; 
				rsp.x = point3d.at<double>(0) - (float) bdata.left;
				rsp.y = (float) bdata.top - point3d.at<double>(1);
				resultPs.push_back(rsp);
				//将GPS坐标从度转成米
				Dpt.x = LON;
				Dpt.y = LAT;
				degree2meter_multi(baseMps,baseDps,Dpt,Mpt);
				//将GPS坐标转到基准图坐标（基准图分辨率是1米/像素）
				gps.x = Mpt.x - (float) bdata.left;
				gps.y = (float) bdata.top - Mpt.y;
				GPSPs.push_back(gps);

				//计算平均匹配误差，和匹配误差超过100米的次数
				if (sqrt(pow(gps.x - rsp.x,2)+pow(gps.y - rsp.y,2)) < 100)
				{
					ave_error += sqrt(pow(gps.x - rsp.x,2)+pow(gps.y - rsp.y,2));
					cn++;
				}else
				{
					error_cn++;
				}
				//按格式保存定位结果到日志文件
				fprintf(logFile,"%d %f %f %f %f %f %f %f %f %f %f %f %f\n",IMG_CNT,UTC,matchLon,matchLat,matchH,LON,LAT,ALT,rsp.x,rsp.y,gps.x,gps.y,t);
				srcps.clear();dstps.clear(); objectPoints.clear(); imagePoints.clear();

			}
		}
	}
	fprintf(logFile,"平均水平位置误差:%f,误差超过100米次数:%d \n",ave_error / (double)cn, error_cn);
	Mat copyImg;
	cvtColor(bdata.baseImg,copyImg,CV_GRAY2BGR);
	//将GPS和匹配定位结果画到基准图并保存
	Drawps(copyImg,resultPs,5,Scalar(0,0,255));
	Drawps(copyImg,GPSPs,5,Scalar(0,255,0));
	imwrite("results.jpg",copyImg);
	fclose(logFile);
	fclose(fp);
}