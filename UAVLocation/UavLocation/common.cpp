#include "stdafx.h"
#include "common.h"
#include <stdio.h>
//字符串分割函数

void getFiles( string path, vector<string>& files )  
{  
	//文件句柄  
	long   hFile   =   0;  
	//文件信息  
	struct _finddata_t fileinfo;  
	string p;  
	if((hFile = _findfirst(p.assign(path).append("\\*").c_str(),&fileinfo)) !=  -1)  
	{  
		do  
		{  
			//如果是目录,迭代之  
			//如果不是,加入列表  
			if((fileinfo.attrib &  _A_SUBDIR))  
			{  
				//do nothing
				//	if(strcmp(fileinfo.name,".") != 0  &&  strcmp(fileinfo.name,"..") != 0)  
				//	getFiles( p.assign(path).append("\\").append(fileinfo.name), files );  
			}  
			else  
			{  
				string fname = fileinfo.name;
				if (fname.substr(fname.size()-3) == "bmp")
				{
					//files.push_back(p.assign(path).append("\\").append(fileinfo.name) );
					files.push_back(fileinfo.name);
				}
			}  
		}while(_findnext(hFile, &fileinfo)  == 0);  
		_findclose(hFile);  
	}  
} 

double str2double(string str)
{
	double v;
	istringstream sst(str);
	sst>>v;
	return v;
}

std::vector<std::string> strsplit(std::string str,std::string pattern)
{
	std::string::size_type pos;
	std::vector<std::string> result;
	str+=pattern;//扩展字符串以方便操作
	unsigned int size = str.size();

	for(unsigned int i=0; i<size; i++)
	{
		pos=str.find(pattern,i);
		if(pos<size)
		{
			std::string s=str.substr(i,pos-i);
			result.push_back(s);
			i=pos+pattern.size()-1;
		}
	}
	return result;
}

void MyErrorExit(const char * message)
{
	fprintf(stderr, message);
	fflush(stdin);
	getchar();
	exit(1);
}

void Drawcorss(Mat img, int x, int y, int cr)
{
	cv::line(img,cvPoint(x-cr,y),cvPoint(x+cr,y),cvScalarAll(255),1);
	cv::line(img,cvPoint(x,y-cr),cvPoint(x,y+cr),cvScalarAll(255),1);
}

typedef cv::Point3_<unsigned char> Pixel;

void BYTE2Mat(unsigned char *imgData, Mat &mimg)
{
	if (mimg.channels() == 3)
	{
		for (int i = 0; i < mimg.rows; i++)
		{
			Pixel* ptr = mimg.ptr<Pixel>(i);
			memcpy(ptr, imgData + i * mimg.cols * mimg.channels(), mimg.cols * mimg.channels());
		}
	}
	else
	{
		for (int i = 0; i < mimg.rows; i++)
		{
			unsigned char* ptr = mimg.ptr<unsigned char>(i);
			memcpy(ptr, imgData + i * mimg.cols * mimg.channels(), mimg.cols * mimg.channels());
		}
	}
}

void Mat2CBYTE(Mat mimg, unsigned char * imgData)
{
	if (mimg.channels() == 3)
	{
		for (int i = 0; i < mimg.rows; i++)
		{
			Pixel* ptr = mimg.ptr<Pixel>(i);
			memcpy(imgData + i *  mimg.cols * mimg.channels(), ptr, mimg.cols * mimg.channels());
		}
	}
	else
	{
		for (int i = 0; i < mimg.rows; i++)
		{
			unsigned char* ptr = mimg.ptr<unsigned char>(i);
			memcpy(imgData + i *  mimg.cols * mimg.channels(), ptr, mimg.cols * mimg.channels());
		}
	}
}
