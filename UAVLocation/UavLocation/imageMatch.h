#ifndef __IMAGE_MATCH__
#define __IMAGE_MATCH__

//

#ifdef OPENCV_DLL_API
#define OPENCV_DLL_API __declspec(dllexport)
#else
#define OPENCV_DLL_API extern "C" _declspec(dllexport)
#endif

//基准图结构
struct BaseData{
	Mat baseImg;  //基准图
	//基准图左上角的地理坐标（米）
	double top;		
	double left;
};

//初始化匹配查找表，匹配前调用一次
OPENCV_DLL_API void bits_count_tab_init();

//功能：异源图像匹配
//输入：实时图的mask,校正后的实时图(Greal),基准图（EGbase）,匹配点在实时图上的位置（srcps）
//输出：匹配点在基准图上的位置（dstps）
OPENCV_DLL_API void ImageMatching_PQHOG(Mat Mask,Mat Greal,Mat EGbase,\
	std::vector<cv::Point2f> & srcps,std::vector<cv::Point2f> & dstps);

#endif 